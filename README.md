Basic test code for controlling a [T-Motor](https://uav-en.tmotor.com) [AK80-series](https://store.cubemars.com/category.php?id=122) motor via the CAN bus.

Borrows code from [can-utils](https://github.com/linux-can/can-utils).

Basic usage:

```
make
./tmotor can0
```

If it can talk to the motor _it may cause the motor to spin_. Beware!

I highly recommend having a copy of `candump` running somewhere to monitor traffic.
