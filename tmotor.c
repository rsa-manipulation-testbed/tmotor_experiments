

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "can-utils/lib.h"

#include <iostream>

const float P_MIN = -95.5;
const float P_MAX = 95.5;

const float V_MIN = -30;
const float V_MAX = 30;

const float T_MIN = -18;
const float T_MAX = 18;

const float Kp_MIN = 0;
const float Kp_MAX = 500;

const float Kd_MIN = 0;
const float Kd_MAX = 5;

static int float_to_uint( float x, float x_min, float x_max, unsigned int bits ) 
{
    float span = x_max - x_min;
    
    if( x < x_min ) 
        x = x_min;
    else if( x > x_max )
        x = x_max;

    return (int)( (x-x_min) * ((float)((1<<bits)-1)/span));
}

static float uint_to_float( int x, float x_min, float x_max, int bits ) 
{
    float span = x_max = x_min;
    float offset = x_min;

    return( (float)x)*span / ((float)((1<<bits)-1)) + offset;
}


void pack_motor_on_frame( int id, struct can_frame *frame ) 
{
    frame->can_id = id;

    frame->can_dlc = 8;
    for( int i = 0; i < 7; i++ ) {
        frame->data[i] = 0xFF;
    }
    frame->data[7] = 0xFC;
}

void pack_motor_off_frame( int id, struct can_frame *frame ) 
{
    frame->can_id = id;
    frame->can_dlc = 8;

    for( int i = 0; i < 7; i++ ) {
        frame->data[i] = 0xFF;
    }
    frame->data[7] = 0xFD;
}


void pack_cmd( int id, struct can_frame *frame, float p_des, float v_des, float kp, float kd, float t_ff ) 
{

    frame->can_id = id;
    frame->can_dlc = 8;

    p_des = fminf( fmaxf( P_MIN, p_des), P_MAX);
    v_des = fminf( fmaxf( V_MIN, v_des), V_MAX);
    kp = fminf(fmax(Kp_MIN,kp),Kp_MAX);
    kd = fminf(fmaxf(Kd_MIN,kd),Kd_MAX);
    t_ff = fminf(fmaxf(T_MIN,t_ff),T_MAX);

    // Convert floats to unsinged int
    const int p_int  = float_to_uint(p_des,P_MIN,P_MAX,16);
    const int v_int  = float_to_uint(v_des,V_MIN,V_MAX,12);
    const int kp_int = float_to_uint(kp, Kp_MIN, Kp_MAX, 12);
    const int kd_int = float_to_uint(kd, Kd_MIN, Kd_MAX, 12);
    const int t_int  = float_to_uint(t_ff, T_MIN, T_MAX, 12);

    // Back ints into can buffer
    frame->data[0] = p_int>>8;          // position 8-H
    frame->data[1] = p_int&0xFF;        // position 8-L

    frame->data[2] = v_int>>4;          // speed 8H
    frame->data[3] = ((v_int&0x0F)<<4) | (kp_int >>8);   // speed-4L KP-8H
    frame->data[4] = kp_int&0xFF;       // KP 8-L
    frame->data[5] = kd_int>>4;         // Kd 8-H
    frame->data[6] = ((kd_int&0x0F)<<4) | (t_int>>8);    // KP 4-L torque 4-H
    frame->data[7] = t_int&0xFF;        // torque 8-L

}   

void unpack_reply( uint8_t *msg ) 
{
    
    int id = msg[0];        // Driver ID number
    int p_int = ( msg[1<<8] )| msg[2];          // Motor position
    int v_int = ( msg[3]<<4) | msg[4]>>4;       // Motor speed
    int i_int = ((msg[4]&0x0F)<<8) | msg[5];    // Motor torque data

    float p = uint_to_float( p_int, P_MIN, P_MAX, 16);
    float v = uint_to_float( v_int, V_MIN, V_MAX, 12);
    float i = uint_to_float( i_int, T_MIN, T_MAX, 12 );

	std::cout <<"==============" << std::endl;
    std::cout << "Id: " << id << std::endl;
    std::cout << "P:  " << p << std::endl;
    std::cout << "V:  " << v << std::endl;
    std::cout << "I:  " << i << std::endl;

}

int read_reply( int fd ) 
{
	struct can_frame frame;

    int nbytes = read(fd, &frame, sizeof(struct can_frame));
    if (nbytes < 0) {
            perror("CAN raw socket read");
            return nbytes;
    }

    /* paranoid check ... */
    if (nbytes < sizeof(struct can_frame)) {
            fprintf(stderr, "read: incomplete CAN frame\n");
            return nbytes;
    }

    unpack_reply( frame.data );

	return nbytes;
}


int main(int argc, char **argv)
{
	int s; /* can raw socket */ 
	const int required_mtu = sizeof(struct can_frame);
	int mtu;
	int enable_canfd = 1;
	struct sockaddr_can addr;
	struct can_frame frame;
	struct ifreq ifr;

    const int id = 1;


	/* check command line options */
	if (argc != 2) {
		//print_usage(argv[0]);
        std::cerr << "Wrong number of args." << std::endl;
		return 1;
	}

	// /* parse CAN frame */
	// required_mtu = parse_canframe(argv[2], &frame);
	// if (!required_mtu){
	// 	fprintf(stderr, "\nWrong CAN-frame format!\n\n");
	// 	print_usage(argv[0]);
	// 	return 1;
	// }

	/* open socket */
	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
		return 1;
	}

	// Set socket timeout
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    std::cerr << "Opening can device " << argv[1] << std::endl;
	strncpy(ifr.ifr_name, argv[1], IFNAMSIZ - 1);
	ifr.ifr_name[IFNAMSIZ - 1] = '\0';
	ifr.ifr_ifindex = if_nametoindex(ifr.ifr_name);
	if (!ifr.ifr_ifindex) {
		perror("if_nametoindex");
		return 1;
	}

    // strncpy(ifr.ifr_name, argv[1], IFNAMSIZ - 1);
    // ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (required_mtu > (int)CAN_MTU) {

		/* check if the frame fits into the CAN netdevice */
		if (ioctl(s, SIOCGIFMTU, &ifr) < 0) {
			perror("SIOCGIFMTU");
			return 1;
		}
		mtu = ifr.ifr_mtu;

		// if (mtu != CANFD_MTU) {
		// 	printf("CAN interface is not CAN FD capable - sorry.\n");
		// 	return 1;
		// }

		// /* interface is ok - try to switch the socket into CAN FD mode */
		// if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
		// 	       &enable_canfd, sizeof(enable_canfd))){
		// 	printf("error when enabling CAN FD support\n");
		// 	return 1;
		// }

		/* ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64 */
//		frame.len = can_fd_dlc2len(can_fd_len2dlc(frame.len));
	}

	/* disable default receive filter on this RAW socket */
	/* This is obsolete as we do not read from the socket at all, but for */
	/* this reason we can remove the receive list in the Kernel to save a */
	/* little (really a very little!) CPU usage.                          */
	//setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}

    pack_motor_on_frame( id, &frame );

	/* send frame */
	if (write(s, &frame, required_mtu) != required_mtu) {
		perror("write motor on");
		return 1;
	}

	read_reply( s );



    pack_cmd( id, &frame, 0.0, 0.0, 1.0, 0.0, 1.0 );

	/* send frame */
	if (write(s, &frame, required_mtu) != required_mtu) {
		perror("write motor on");
		return 1;
	}

	read_reply( s );


    pack_motor_off_frame( id, &frame );

	/* send frame */
	if (write(s, &frame, required_mtu) != required_mtu) {
		perror("write motor off");
		return 1;
	}

	read_reply( s );


	close(s);

	return 0;
}
